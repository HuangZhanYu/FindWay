﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EasyTouchControl : MonoBehaviour 
{
    public CharacterController charcterControll; //控制的角色
    public Transform thumb; // 滑动的uitexture
    public Transform control;
    public float distanceLimit = 100f; //长度限制
    public Camera uiCamera;

    private Vector3 direction = Vector3.zero;//移动的方向
    private float angle = 0; //移动的角度
    private bool pressState = false;
    private Vector3 touchPosition = Vector3.zero; //手指或者是鼠标触摸的位置
    void Awake()
    {
        Debug.Log(Vector3.forward);
        control.gameObject.SetActive(false);
    }
	
    /// <summary>
    /// 刷新thumb坐标
    /// </summary>
    private void updateThumbPos()
    {
        Vector3 worldPos = touchPosition;
        Vector3 localPos = control.InverseTransformPoint(worldPos);
        float dis = Vector3.Distance(localPos, Vector3.zero);
        Vector3 direction = localPos.normalized;
        if (dis >= distanceLimit)
        {
            dis = distanceLimit;
            localPos = direction * dis;
        }
        thumb.localPosition = localPos;
    }

    /// <summary>
    /// 获取鼠标或者是手指触摸位置
    /// </summary>
    /// <returns></returns>
    private Vector3 getTouchPostion()
    {
        touchPosition = uiCamera.ScreenToWorldPoint(UICamera.lastTouchPosition);
        Vector3 localPos = control.InverseTransformPoint(touchPosition);
        float radian = Mathf.Atan2(localPos.x, localPos.y); // 计算x与Y之间的弧度夹角
        float angle = radian * Mathf.Rad2Deg; //弧度转角度
        this.direction = localPos.normalized;
        this.angle = angle;
        return touchPosition;
    }

    private void move()
    {
        direction = Vector3.forward; //移动的方向是向前移动
        charcterControll.transform.localEulerAngles = Vector3.up * angle; //设置旋转角度
        //charcterControll.transform.eulerAngles = charcterControll.transform.TransformDirection(Vector3.up * -angle); // 
        direction = charcterControll.transform.TransformDirection(direction); //局部坐标转成世界坐标 局部坐标的向前和世界坐标的向前是不一样的 所以需要转换
        direction *= 7.5f;
        Debug.Log(direction);
        charcterControll.Move(direction * Time.deltaTime);
    }

    private void OnDrag(Vector2 delta)
    {
        getTouchPostion();
        updateThumbPos();
        Debug.Log(">>>>>>>滑动了");
    }

    private void OnPress(bool state)
    {
        control.gameObject.SetActive(state);
        if (state)
        {
            getTouchPostion();
            updateThumbPos();
        }
        else
        {
            thumb.localPosition = Vector3.zero;
            angle = 0;
            direction = Vector3.zero;
        }
        pressState = state;
    }

	void Update () 
    {
        if (pressState == true)
        {
            if (angle != 0 && direction != Vector3.zero)
            {
                move();
            }
        }
        
	}
}
