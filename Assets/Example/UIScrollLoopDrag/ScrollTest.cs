﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollTest : MonoBehaviour {


    public UIScrowLoop scrowllLoop;
    public int index;//需要定位的数据index 从0开始

    public int itemCount = 12; // item的数量
    public int dataCount = 24; //数据的数量
    public GameObject btn;
    void Awake()
    {
        scrowllLoop.renderItem = onRenderItem;
        scrowllLoop.initItem(dataCount, itemCount);
        UIEventListener.Get(btn).onClick = onClickBtn;
    }

    private void onRenderItem(Transform item,int itemIndex, int index)
    {
        UILabel label = item.Find("index").GetComponent<UILabel>();
        label.text = index.ToString();
    }

    private void onClickBtn(GameObject go)
    {
        scrowllLoop.renderItemByIndex(index);
    }
    
}
