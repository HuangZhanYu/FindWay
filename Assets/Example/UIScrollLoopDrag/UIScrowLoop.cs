﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIScrowLoop : MonoBehaviour
{
    /// <summary>
    /// item , item下标 , 数据下标 （都是从0开始）
    /// </summary>
    public Action<Transform,int,int> renderItem; //渲染item
    public Action renderAllItemCallBack; // 渲染完所有item的回调

    public UIGrid grid;// 排序组件
    public GameObject itemPrefab; //item预制体
    public UIScrollView scrollView; //滑动组件

    private int dataCount; //数据个数
    private List<Transform> childers; //item列表
    private UIPanel panel; //滑动的panel组件

    private float width { get { return grid.cellWidth; } } // item宽度
    private float height { get { return grid.cellHeight; } } //item高度

    //If the arrangement is horizontal, this denotes the number of columns.
    // If the arrangement is vertical, this stands for the number of rows.
    private int maxPerLine { get { return grid.maxPerLine; } }
    private int rows = 1; //行数 (预制体所占的总行数)
    private int columns = 1; //列数 (预制体所占的总列数)
    private float extents = 0; //所有预制体所占长度或者是高度的一半 用在循环的时候计算item的坐标
    private int itemCount = 10; //预制体item的数量
    void Awake()
    {
        
    }
    /// <summary>
    ///创建item
    /// </summary>
    private void creatItem()
    {
        if (itemCount == 0)
        {
            itemCount = grid.transform.childCount;
        }
        else
        {
            int childCount = grid.transform.childCount;
            int count = itemCount - childCount;
            for (int i = 0; i < count; i++)
            {
                GameObject go = GameObject.Instantiate(itemPrefab) as GameObject;
                go.transform.SetParent(grid.transform, false);
                childers.Add(go.transform);
            }
        }
        grid.pivot = UIWidget.Pivot.TopLeft;  //强制锚点为左上
        grid.Reposition();
    }
    /// <summary>
    /// 初始化数据
    /// </summary>
    public void initData ()
    {
        int itemCount = childers.Count;
        if (scrollView.movement == UIScrollView.Movement.Horizontal)
        {
            rows = maxPerLine;
            columns = itemCount / maxPerLine;
            extents = columns * width * 0.5f;
        }
        else
        {
            columns = maxPerLine;
            rows = itemCount / maxPerLine;
            extents = rows * height * 0.5f;
        }
        updateAllItem();
    }
    /// <summary>
    /// 刷新所有的item信息
    /// </summary>
    public void updateAllItem()
    {
        for (int i = 0; i < itemCount; i++)
        {
            Transform tem = childers[i];
            tem.name = i.ToString();
            Vector2 pos = tem.localPosition;
            int index = getRealIndexByPos(pos);
            if (index < dataCount)
            {
                updateItem(tem, i, index);
            }
        }
    }
    /// <summary>
    /// 重置到最初状态
    /// </summary>
    public void resetToStart()
    {
        Vector3 initPos = panel.transform.localPosition + new Vector3(panel.clipOffset.x, panel.clipOffset.y,0);
        panel.transform.localPosition = initPos;
        panel.clipOffset = Vector2.zero;
        grid.repositionNow = true;
        updateAllItem();
    }
    /// <summary>
    /// 初始化item 
    /// </summary>
    /// <param name="dataCount">数据数量</param>
    /// <param name="itemCount">预制体数量</param>
    public void initItem(int dataCount, int itemCount = 0)
    {
        this.dataCount = dataCount;
        this.itemCount = itemCount;
        childers = new List<Transform>();
        panel = scrollView.transform.GetComponent<UIPanel>();
        panel.onClipMove = onMove;
        creatItem();
        initData();
    }
    /// <summary>
    /// 滑动回调
    /// </summary>
    /// <param name="panel"></param>
    private void onMove(UIPanel panel)
    {
        Vector3[] corners = panel.worldCorners;

        for (int i = 0; i < 4; ++i)
        {
            Vector3 v = corners[i];
            v = grid.transform.InverseTransformPoint(v);
            corners[i] = v;
        }

        Vector3[] localCorners = corners;
        Vector3 center = (localCorners[0] + localCorners[2]) * 0.5f; // 边角的中心点坐标
        bool allWithinRange = true;
        //0:bottom - left    1:top - left     2:top - right     3:bottom - right
        if (scrollView.movement == UIScrollView.Movement.Horizontal)
        {
            float min = localCorners[0].x - width;
            float max = localCorners[2].x + width;
            int count = childers.Count;
            for (int i = 0; i < count; i++)
            {
                Transform item = childers[i];
                Vector3 localPos = item.localPosition;
                float distance = localPos.x - center.x;  //用来判断是在左边还是右边  计算item坐标离中心的距离

                Vector2 pos = item.localPosition;
                int realIndex = 0;
                if (distance < -extents || distance > extents)
                {
                    if (distance < -extents) // 向左拉的时候(向右移动)
                    {
                        pos.x += extents * 2;
                        realIndex = getRealIndexByPos(pos);
                    }
                    else if (distance > extents) //向右拉 （向左移动）
                    {
                        pos.x -= extents * 2;
                        realIndex = getRealIndexByPos(pos);
                    }
                    if (realIndex >=0 && realIndex < dataCount)
                    {
                        item.localPosition = pos;
                        updateItem(item,i, realIndex);
                    }
                    else allWithinRange = false;
                }
                
            }
        }
        else if (scrollView.movement == UIScrollView.Movement.Vertical)
        {
            float min = localCorners[0].x - width;
            float max = localCorners[2].x + width;
            int count = childers.Count;
            for (int i = 0; i < count; i++)
            {
                Transform item = childers[i];
                Vector3 localPos = item.localPosition;
                float distance = localPos.y - center.y;  //用来判断是在上边还是下边  计算item坐标离中心的距离

                Vector2 pos = item.localPosition;
                int realIndex = 0;
                if (distance < -extents || distance > extents)
                {
                    if (distance < -extents) // 向左拉的时候(向右移动)
                    {
                        pos.y += extents * 2;
                        realIndex = getRealIndexByPos(pos);
                    }
                    else if (distance > extents) //向右拉 （向左移动）
                    {
                        pos.y -= extents * 2;
                        realIndex = getRealIndexByPos(pos);
                    }
                    if (realIndex >= 0 && realIndex < dataCount)
                    {
                        item.localPosition = pos;
                        updateItem(item, i, realIndex);
                    }
                    else allWithinRange = false;
                }
            }
        }
        onRenderCompelte();
        scrollView.restrictWithinPanel = !allWithinRange;
        scrollView.InvalidateBounds();
    }
    /// <summary>
    /// 根据item的局部坐标来获取对应的数据下标
    /// </summary>
    /// <param name="pos"></param>
    /// <returns></returns>
    private int getRealIndexByPos(Vector2 pos)
    {
        int realIndex = 0;  // 0 - dataCount-1
        int currentRows = (int)(-pos.y / height) ; //行数
        int currentColumns = (int)(pos.x / width); //列数

        if (scrollView.movement == UIScrollView.Movement.Horizontal)
        {
            realIndex = currentRows + maxPerLine * currentColumns;
        }
        else
        {
            realIndex = currentRows * maxPerLine + currentColumns;
        }
        return realIndex;
    }
    /// <summary>
    ///  根据数据index来定位到需要显示的数据
    /// </summary>
    /// <param name="dataIndex">数据index 从0开始算</param>
    public void renderItemByIndex(int dataIndex)
    {
        if (dataIndex < 0)
        {
            dataIndex = 0;
        }
        else if(dataIndex >= dataCount)
        {
            dataIndex = dataCount - 1;
        }
		int currentColumns = 0; //列数 从0开始
        int currentRows = 0; //行数 从0开始
        int maxRows = 0; //最大行数
        int maxColumns = 0; //最大列数
        float posY = 0; 
        float posX = 0;
        int showCountItem = rows * columns;
        if (scrollView.movement == UIScrollView.Movement.Horizontal)
        {
            currentColumns = dataIndex / maxPerLine;
            maxColumns = (dataCount - 1) / maxPerLine;
            currentRows = dataIndex % maxPerLine;
           
            int startColumns = 0; //开始的列数
            int offsetColums = 0; //偏移的列数
            int maxShowColumns = (int)(panel.width / width);

            if (currentColumns + maxShowColumns / 2 < maxShowColumns)//如果要显示的列数小于屏幕显示的最大列数 就直接从0列开始显示
            {
                startColumns = 0;
                offsetColums = startColumns;
            }
            else if (currentColumns + maxShowColumns/2 >= maxColumns) //如果当前显示的列数加上屏幕显示的最大列数大于最大的列数就从 最大列数-屏幕显示列数 开始显示
            {
                startColumns = maxColumns - columns + 1;
                offsetColums = maxColumns - maxShowColumns + 1;
            }
            else //正常显示 这里可以改成- N 
            {
                //（很多时候坑逼策划会让你居中显示， 下面代码可以改成 startColumns = currentColumns - (columns / 2) +1
                startColumns = currentColumns - 1;
                offsetColums = startColumns;
            }
            int line = -1;

            //计算裁剪区域offset以及panel的坐标
            Vector2 offset = new Vector2((offsetColums) * width, 0);
            float x = panel.transform.localPosition.x + panel.clipOffset.x;
            panel.clipOffset = offset;
            panel.transform.localPosition = new Vector3(x - offset.x, 0, 0);

            for (int i = 0; i < showCountItem; i++)
            {
                Transform item = childers[i];

                if (i % rows == 0) //下一列了
                {
                    line += 1;
                }
                int column = (startColumns + line);
                //if (column > maxColumns)
                //{
                //    break;//超标了 直接退出
                //}

                posX = column * width;
                posY = i % rows * -height;
                Vector3 newPos = new Vector3(posX,posY);
                item.localPosition = newPos;
                int realIndex = getRealIndexByPos(newPos);
                updateItem(item, i, realIndex);
            }
        }
        else
        {
            currentRows = dataIndex / maxPerLine;
            maxRows = (dataCount - 1) / maxPerLine;
            currentColumns = dataIndex % maxPerLine;

            int startRows = 0; //开始的行数
            int offsetRows = 0; // 偏移的行数
            int maxShowRows = (int)(panel.height / height); //裁剪区域能显示的最大行数
            if (currentRows + maxShowRows / 2 < maxShowRows)//
            {
                startRows = 0;
                offsetRows = startRows;
            }
            else if (currentRows + maxShowRows/2 >= maxRows)
            {
                startRows = maxRows - rows + 1;
                offsetRows = maxRows - maxShowRows +1;
            }
            else
            {
                startRows = currentRows - 1;
                offsetRows = startRows;
            }
            int line = -1;

            //计算裁剪以及panel的坐标
            Vector2 offset = new Vector2(0, offsetRows * -height);
            float y = panel.transform.localPosition.y + panel.clipOffset.y;
            panel.clipOffset = offset;
            panel.transform.localPosition = new Vector3(panel.transform.localPosition.x, y - offset.y, 0);


            for (int i = 0; i < showCountItem; i++)
            {
                Transform item = childers[i];
                if (i % columns == 0) //下一行了
                {
                    line += 1;
                }
                int row = (startRows + line);
                if (row > maxRows)
                {
                    break;//超标了 直接退出
                }
                posY = row * -height;
                posX = i % columns * width;

                Vector3 newPos = new Vector3(posX, posY);
                item.localPosition = newPos;
                int realIndex = getRealIndexByPos(newPos);
                updateItem(item, i, realIndex);
            }
        }

        onRenderCompelte();
    }
    /// <summary>
    /// 渲染item
    /// </summary>
    /// <param name="item">渲染对应的item</param>
    /// <param name="itemIndex">渲染对应的itemindex</param>
    /// <param name="index">数据index</param>
    private void updateItem(Transform item,int itemIndex, int index)
    {
        if (renderItem != null)
        {
            renderItem(item,itemIndex,index);
        }
    }
    /// <summary>
    /// 渲染结束的事件
    /// </summary>
    private void onRenderCompelte()
    {
        if (renderAllItemCallBack != null)
        {
            renderAllItemCallBack();
        }
    }
}
